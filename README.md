## Základní použití driveru pro Murata_SyChip_ABZ modul

Vytvořená knihovna disponuje několika funkcemi, které umožňují správu, nastavení a čtení dat z modulu.

Příklad použití knihovny:

```c
// Inkludování potřebných knihoven
#include "cesta/bin_buffer/bin_buffer.h"
#include "cesta/murata_sychip_abz/murata_sychip_abz.h"

murata_sychip_abz_t murata; // Vytvoření struktury nesoucí veškeré informace o modulu

// Vytvoření funkce pro callback timeru
uint8_t tc0_timer_callback_flag = 0;

void timer_callback(){
    timer_callback_flag = 1;
}

// Vytvoření funkce pro zapnutí power switche M2

// Vytvoření funkce pro vypnutí power switche M2

// Vytvoření funkce pro callback odesílání dat

// Vytvoření funkce pro callback přijímání dat

// Vytvoření funkce pro delay

int main(void){
    // Inicializace systémových knihoven
    

    // Zaregistrování timeru pro periodické odesílání dat
    TimerCallbackRegister(timer_callback);
    TimerStart();


    // Prvotní nastavení modulu
    murata._task_state = MURATA_SYCHIP_ABZ_TASK_STATE_INIT;
    
    murata.configuration.clkFrequency = 0;
    murata.configuration.power_mode   = MURATA_SYCHIP_ABZ_POWER_MODE_ON;
    
    murata.configuration.rx_callback_funcptr = module_usart_read_callback;
    murata.configuration.tx_callback_funcptr = module_usart_write_callback;
    
    murata.configuration.serial_setup.baudRate  = MURATA_SYCHIP_ABZ_DEFAULT_BAUDRATE;
    murata.configuration.serial_setup.dataWidth = USART_DATA_8_BIT;
    murata.configuration.serial_setup.parity    = USART_PARITY_NONE;
    murata.configuration.serial_setup.stopBits  = USART_STOP_1_BIT;
    
    murata.power_switch_enable_funcptr  = module_power_switch_enable;
    murata.power_switch_disable_funcptr = module_power_switch_disable;
    
    murata.rw_interaction = MURATA_SYCHIP_ABZ_RW_INTERACTION_NONE;


    while(true){

        murata_sychip_abz_Task(&murata);

        if(timer_callback_flag == 1){
            murata_sychip_abz_write_start(&murata, (uint8_t *)"AT\r", 3);
            timer_callback_flag = 0;
        }

        if(murata_sychip_abz_is_write_done(&murata, &test_len) == 1){
            
            user_delay_ms(100);
            
            printf("Length of posted data: %d\r\n", test_len);

            murata_sychip_abz_read_start(&murata, 128);
        }

        if(murata_sychip_abz_is_read_done(&murata, (uint8_t *)test_buffer, &test_len) == 1){
            printf("Length of received data: %d  and received data: ", test_len);
            
            for(uint8_t i = 0; i < test_len; i++) printf("%c", (char)test_buffer[i]);
            printf("\r\n");
        }

    }
    
    return 0;
}
```
