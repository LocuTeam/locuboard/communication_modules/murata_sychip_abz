/*
 * murata_sychip_abz driver � 2021 by Miroslav Soukup is licensed under 
 * Attribution-NonCommercial-NoDerivatives 4.0 International. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
*/

/*
  @File Name
    murata_sychip_abz.c

  @Summary
    Driver for LoRa communication card MuRata SyChip ABZ.
*/


/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */

#include "murata_sychip_abz.h"

#include "config/default/peripheral/sercom/usart/plib_sercom0_usart.h"

#include "../bin_buffer/bin_buffer.h"

/* ************************************************************************** */
/* Section: File Scope or Global Data                                         */
/* ************************************************************************** */

char _murata_sychip_abz_version[] = "00.01";


/* ************************************************************************** */
// Section: Local Functions                                                   */
/* ************************************************************************** */





/* ************************************************************************** */
// Section: Interface Functions                                               */
/* ************************************************************************** */


MURATA_SYCHIP_ABZ_TASK_STATE_e murata_sychip_abz_Task(murata_sychip_abz_t *me){
    switch(me->_task_state){
        case MURATA_SYCHIP_ABZ_TASK_STATE_INIT:
            
//            printf("\r\n -- MURATA TASK INIT -- \r\n");
            
            SERCOM0_USART_ReadCallbackRegister(me->configuration.rx_callback_funcptr, (uintptr_t)me);
            SERCOM0_USART_WriteCallbackRegister(me->configuration.tx_callback_funcptr, (uintptr_t)me);

            SERCOM0_USART_SerialSetup(&me->configuration.serial_setup, me->configuration.clkFrequency);
            
            if(me->configuration.power_mode == MURATA_SYCHIP_ABZ_POWER_MODE_OFF){
                me->_task_state = MURATA_SYCHIP_ABZ_TASK_STATE_READ_LISTEN_STOP;
            }
            else if(me->configuration.power_mode == MURATA_SYCHIP_ABZ_POWER_MODE_ON){
                me->_task_state = MURATA_SYCHIP_ABZ_TASK_STATE_TURN_POWER_ON;
            }
            else{
                // ERROR
            }
            
        break;
        
        case MURATA_SYCHIP_ABZ_TASK_STATE_TURN_POWER_ON:
//            printf("\r\n -- MURATA TASK TURN_POWER_ON -- \r\n");
            
            me->power_switch_enable_funcptr(me);
            me->_task_state = MURATA_SYCHIP_ABZ_TASK_STATE_READ_LISTEN_BEGIN;
        break;
        
        case MURATA_SYCHIP_ABZ_TASK_STATE_READ_LISTEN_BEGIN:
//            printf("\r\n -- MURATA TASK READ_LISTEN_BEGIN -- \r\n");
            
            SERCOM0_USART_TransmitterEnable();
            SERCOM0_USART_ReceiverEnable();
            
            SERCOM0_USART_Read((void *)&me->recv_char, 1); // Begin module USART reading
            
            me->_task_state = MURATA_SYCHIP_ABZ_TASK_STATE_STANDBY;
        break;
        
        case MURATA_SYCHIP_ABZ_TASK_STATE_STANDBY:
            if(me->configuration.power_mode == MURATA_SYCHIP_ABZ_POWER_MODE_OFF){
                me->_task_state = MURATA_SYCHIP_ABZ_TASK_STATE_READ_LISTEN_STOP;
            }
            else if(me->rw_interaction == MURATA_SYCHIP_ABZ_RW_INTERACTION_READ){
                me->_task_state = MURATA_SYCHIP_ABZ_TASK_STATE_READING_BEGIN;
            }
            else if(me->rw_interaction == MURATA_SYCHIP_ABZ_RW_INTERACTION_WRITE){
                me->_task_state = MURATA_SYCHIP_ABZ_TASK_STATE_WRITING_BEGIN;
            }
        break;
        
            
        case MURATA_SYCHIP_ABZ_TASK_STATE_READING_BEGIN:
//            printf("\r\n -- MURATA TASK READING_BEGIN -- \r\n");
            me->_task_state = MURATA_SYCHIP_ABZ_TASK_STATE_READING;
        break;
        
        case MURATA_SYCHIP_ABZ_TASK_STATE_READING:
            
            me->read_len = bin_buffer_get_data(&me->recv_bin_buff, me->read_buff, me->read_len);
            
            // me->write_flag (dokud se nevyvola n krat interrupt tak se porad cte -> nutne pridat)
            
            me->_task_state = MURATA_SYCHIP_ABZ_TASK_STATE_READING_DONE;
        break;
        
        case MURATA_SYCHIP_ABZ_TASK_STATE_READING_DONE:
//            printf("\r\n -- MURATA TASK READING_DONE -- \r\n");
            
            me->read_done_flag = 1;
            
            me->rw_interaction = MURATA_SYCHIP_ABZ_RW_INTERACTION_NONE;
            me->_task_state = MURATA_SYCHIP_ABZ_TASK_STATE_STANDBY;
        break;
        
        
        case MURATA_SYCHIP_ABZ_TASK_STATE_WRITING_BEGIN:
//            printf("\r\n -- MURATA TASK WRITING_BEGIN -- \r\n");
            me->_task_state = MURATA_SYCHIP_ABZ_TASK_STATE_WRITING;
        break;
        
        case MURATA_SYCHIP_ABZ_TASK_STATE_WRITING:
            
            SERCOM0_USART_Write((void *)me->write_buff, (size_t)me->write_len);
            
            if(me->write_flag == 1){
                me->write_flag = 0;
                me->_task_state = MURATA_SYCHIP_ABZ_TASK_STATE_WRITING_DONE;
            }
        break;
        
        case MURATA_SYCHIP_ABZ_TASK_STATE_WRITING_DONE:
//            printf("\r\n -- MURATA TASK WRITING_DONE -- \r\n");
            
            me->write_done_flag = 1;
            
            me->rw_interaction = MURATA_SYCHIP_ABZ_RW_INTERACTION_NONE;
            me->_task_state = MURATA_SYCHIP_ABZ_TASK_STATE_STANDBY;
        break;
        
        
        case MURATA_SYCHIP_ABZ_TASK_STATE_READ_LISTEN_STOP:
//            printf("\r\n -- MURATA TASK READ_LISTEN_STOP -- \r\n");
            
            SERCOM0_USART_TransmitterDisable();
            SERCOM0_USART_ReceiverDisable();
            
            me->_task_state = MURATA_SYCHIP_ABZ_TASK_STATE_TURN_POWER_OFF;
        break;
        
        case MURATA_SYCHIP_ABZ_TASK_STATE_TURN_POWER_OFF:
//            printf("\r\n -- MURATA TASK TURN_POWER_OFF -- \r\n");
            
            me->power_switch_disable_funcptr(me);
            
            me->_task_state = MURATA_SYCHIP_ABZ_TASK_STATE_POWER_OFF;
        break;
        
        case MURATA_SYCHIP_ABZ_TASK_STATE_POWER_OFF:
            if(me->configuration.power_mode == MURATA_SYCHIP_ABZ_POWER_MODE_ON){
                me->_task_state = MURATA_SYCHIP_ABZ_TASK_STATE_INIT;
            }
        break;
        
        default:
            // ERR
        break;
    }
    
    return me->_task_state;
}


uint8_t murata_sychip_abz_write_start(murata_sychip_abz_t *me, uint8_t *buff, uint8_t len){
    if((me->configuration.power_mode == MURATA_SYCHIP_ABZ_POWER_MODE_ON) && (me->_task_state == MURATA_SYCHIP_ABZ_TASK_STATE_STANDBY) && (me->rw_interaction == MURATA_SYCHIP_ABZ_RW_INTERACTION_NONE)){
        
        for(uint8_t i = 0; i < len; i++) me->write_buff[i] = *(buff + i);
        me->write_len = len;
        
        me->rw_interaction = MURATA_SYCHIP_ABZ_RW_INTERACTION_WRITE;
        
        return len;
    }
    else{
        return 0;
    }
}

uint8_t murata_sychip_abz_is_write_done(murata_sychip_abz_t *me, uint8_t *len){
    if((me->configuration.power_mode == MURATA_SYCHIP_ABZ_POWER_MODE_ON) && (me->write_done_flag == 1)){

        *len = me->write_len;
        
        me->write_done_flag = 0;
        
        return 1;
    }
    else{
        return 0;
    }
}


uint8_t murata_sychip_abz_read_start(murata_sychip_abz_t *me, uint8_t len){
    if((me->configuration.power_mode == MURATA_SYCHIP_ABZ_POWER_MODE_ON) && (me->_task_state == MURATA_SYCHIP_ABZ_TASK_STATE_STANDBY) && (me->rw_interaction == MURATA_SYCHIP_ABZ_RW_INTERACTION_NONE)){
        
        me->read_len = len;
        
        me->rw_interaction = MURATA_SYCHIP_ABZ_RW_INTERACTION_READ;
        
        return 1;
    }
    else{
        return 0;
    }
}

uint8_t murata_sychip_abz_is_read_done(murata_sychip_abz_t *me, uint8_t *buff, uint8_t *len){
    if((me->configuration.power_mode == MURATA_SYCHIP_ABZ_POWER_MODE_ON) && (me->read_done_flag == 1)){
        
        for(uint8_t i = 0; i < me->read_len; i++) *(buff + i) = me->read_buff[i];
        *len = me->read_len;
        
        me->read_done_flag = 0;
        
        return 1;
    }
    else{
        return 0;
    }
}


char *murata_sychip_abz_get_version(void){
    return _murata_sychip_abz_version;
}


