/*
 * murata_sychip_abz driver � 2021 by Miroslav Soukup is licensed under 
 * Attribution-NonCommercial-NoDerivatives 4.0 International. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
*/

/*
  @File Name
    murata_sychip_abz.h

  @Summary
    Driver for LoRa communication card MuRata SyChip ABZ.
*/


#ifndef _MURATA_SYCHIP_ABZ_H    /* Guard against multiple inclusion */
#define _MURATA_SYCHIP_ABZ_H


/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */

#include <stdint.h>

#include "config/default/peripheral/sercom/usart/plib_sercom0_usart.h"

#include "../bin_buffer/bin_buffer.h"


/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif


    /* ************************************************************************** */
    /* Section: Constants                                                         */
    /* ************************************************************************** */

    #define MURATA_SYCHIP_ABZ_BUFFER_SIZE BIN_BUFFER_SIZE

    #define MURATA_SYCHIP_ABZ_DEFAULT_BAUDRATE __UINT32_C(19200)
    
    
    // *****************************************************************************
    // Section: Data Types
    // *****************************************************************************
    
    
    typedef struct murata_sychip_abz_descriptor murata_sychip_abz_t;
    typedef struct murata_sychip_abz_configuration_descriptor murata_sychip_abz_configuration_t;

    typedef void (*murata_sychip_abz_callback_funcptr)(uintptr_t context);

    typedef void (*murata_sychip_abz_power_switch_enable_funcptr)(murata_sychip_abz_t *me);
    typedef void (*murata_sychip_abz_power_switch_disable_funcptr)(murata_sychip_abz_t *me);
    

    typedef enum{
        MURATA_SYCHIP_ABZ_TASK_STATE_INIT              = 0,
        MURATA_SYCHIP_ABZ_TASK_STATE_TURN_POWER_ON     = 1,
        MURATA_SYCHIP_ABZ_TASK_STATE_READ_LISTEN_BEGIN = 2,
        MURATA_SYCHIP_ABZ_TASK_STATE_STANDBY           = 3,

        MURATA_SYCHIP_ABZ_TASK_STATE_READING_BEGIN     = 4,
        MURATA_SYCHIP_ABZ_TASK_STATE_READING           = 5,
        MURATA_SYCHIP_ABZ_TASK_STATE_READING_DONE      = 6,

        MURATA_SYCHIP_ABZ_TASK_STATE_WRITING_BEGIN     = 7,
        MURATA_SYCHIP_ABZ_TASK_STATE_WRITING           = 8,
        MURATA_SYCHIP_ABZ_TASK_STATE_WRITING_DONE      = 9,

        MURATA_SYCHIP_ABZ_TASK_STATE_READ_LISTEN_STOP  = 10,
        MURATA_SYCHIP_ABZ_TASK_STATE_TURN_POWER_OFF    = 11,
        MURATA_SYCHIP_ABZ_TASK_STATE_POWER_OFF         = 12
    }MURATA_SYCHIP_ABZ_TASK_STATE_e;


    typedef enum{
        MURATA_SYCHIP_ABZ_POWER_MODE_OFF = 0,
        MURATA_SYCHIP_ABZ_POWER_MODE_ON  = 1,
    }MURATA_SYCHIP_ABZ_POWER_MODE_e;


    typedef enum{
        MURATA_SYCHIP_ABZ_RW_INTERACTION_NONE  = 0,
        MURATA_SYCHIP_ABZ_RW_INTERACTION_READ  = 1,
        MURATA_SYCHIP_ABZ_RW_INTERACTION_WRITE = 2
    }MURATA_SYCHIP_ABZ_RW_INTERACTION_e;



    struct murata_sychip_abz_configuration_descriptor{
        MURATA_SYCHIP_ABZ_POWER_MODE_e power_mode;

        murata_sychip_abz_callback_funcptr tx_callback_funcptr;
        murata_sychip_abz_callback_funcptr rx_callback_funcptr;

        USART_SERIAL_SETUP serial_setup;

        uint32_t clkFrequency;
    };


    struct murata_sychip_abz_descriptor{

        murata_sychip_abz_configuration_t configuration;

        murata_sychip_abz_power_switch_enable_funcptr power_switch_enable_funcptr;
        murata_sychip_abz_power_switch_disable_funcptr power_switch_disable_funcptr;

        MURATA_SYCHIP_ABZ_RW_INTERACTION_e rw_interaction;

        MURATA_SYCHIP_ABZ_TASK_STATE_e _task_state;

        bin_buffer_t recv_bin_buff;
        uint8_t recv_char;

        uint8_t read_buff[MURATA_SYCHIP_ABZ_BUFFER_SIZE];
        uint8_t write_buff[MURATA_SYCHIP_ABZ_BUFFER_SIZE];

        uint8_t read_done_flag;
        uint8_t write_done_flag;

        uint8_t read_len;
        uint8_t write_len;

        uint8_t write_flag; // maybe useless
        uint8_t write_flag_counter; // need to be implemented
        
        uint8_t read_flag; // maybe useless
        uint8_t read_flag_counter; // need to be implemented
    };


    // *****************************************************************************
    // Section: Interface Functions
    // *****************************************************************************
    
    char *murata_sychip_abz_get_version(void);
    
    MURATA_SYCHIP_ABZ_TASK_STATE_e murata_sychip_abz_Task(murata_sychip_abz_t *me);
    
    uint8_t murata_sychip_abz_write_start(murata_sychip_abz_t *me, uint8_t *buff, uint8_t len);
    
    uint8_t murata_sychip_abz_is_write_done(murata_sychip_abz_t *me, uint8_t *len);
    
    uint8_t murata_sychip_abz_read_start(murata_sychip_abz_t *me, uint8_t len);
    
    uint8_t murata_sychip_abz_is_read_done(murata_sychip_abz_t *me, uint8_t *buff, uint8_t *len);
    

    
    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _MURATA_SYCHIP_ABZ_H */
